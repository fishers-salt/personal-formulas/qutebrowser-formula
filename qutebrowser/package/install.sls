# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as qutebrowser with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

qutebrowser-package-install-removables-removed:
  pkg.removed:
    - pkgs: {{ qutebrowser.removables }}

qutebrowser-package-install-dependencies-installed:
  pkg.installed:
    - pkgs: {{ qutebrowser.dependencies }}
    - require:
      - qutebrowser-package-install-removables-removed

qutebrowser-package-install-packages-installed:
  pkg.installed:
    - pkgs: {{ qutebrowser.pkgs }}
    - require:
      - qutebrowser-package-install-dependencies-installed

{# Can't install qutebrowser in test env - salt creates these files manually #}
{%- set testing_env = qutebrowser.get('testing_env', false) %}
{%- if testing_env %}
qutebrowser-package-install-clean-salt-artifacts:
  cmd.run:
    - name: rm -rf /usr/lib/python3.10/site-packages/jinja2/ && rm -rf /usr/lib/python3.10/site-packages/markupsafe/
    - onfail:
      - pkg: qutebrowser-package-install-packages-installed
    - onfail_stop: false
    - require:
      - qutebrowser-package-install-dependencies-installed
  pkg.installed:
    - pkgs: {{ qutebrowser.pkgs }}
{%- endif %}

{% if salt['pillar.get']('qutebrowser-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_qutebrowser', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

qutebrowser-package-install-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

qutebrowser-package-install-bin-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/bin
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - qutebrowser-package-install-user-{{ name }}-present

qutebrowser-package-install-qutebrowser-profile-script-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/bin/qutebrowser
    - source: {{ files_switch([
                  name ~ '-wrapper-script.sh.tmpl',
                  'wrapper-script.sh.tmpl'],
                lookup='qutebrowser-package-install-qutebrowser-profile-script-' ~ name ~ '-managed',
                )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - qutebrowser-package-install-bin-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
