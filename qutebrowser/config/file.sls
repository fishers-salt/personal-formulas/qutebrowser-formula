# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as qutebrowser with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('qutebrowser-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_qutebrowser', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

qutebrowser-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

qutebrowser-config-file-qutebrowser-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/qutebrowser
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - qutebrowser-config-file-user-{{ name }}-present

qutebrowser-config-file-bookmarks-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/qutebrowser/bookmarks
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - require:
      - qutebrowser-config-file-qutebrowser-dir-{{ name }}-managed

{%- set qutebrowser_user = user.get('qutebrowser', {}) %}

{%- set favourites = qutebrowser_user.get('favourites', {}) %}
qutebrowser-config-file-config-py-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/config.py
    - source: {{ files_switch([
                  name ~ '-config.py.tmpl',
                  'config.py.tmpl'],
                lookup='qutebrowser-config-file-config-py-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        favourites: {{ favourites }}
    - require:
      - qutebrowser-config-file-qutebrowser-dir-{{ name }}-managed

qutebrowser-config-file-keys-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/keys.conf
    - source: {{ files_switch([
                  name ~ '-keys.conf.tmpl',
                  'keys.conf.tmpl'],
                lookup='qutebrowser-config-file-keys-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - qutebrowser-config-file-qutebrowser-dir-{{ name }}-managed

qutebrowser-config-file-config-conf-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/qutebrowser.conf
    - source: {{ files_switch([
                  name ~ '-qutebrowser.conf.tmpl',
                  'qutebrowser.conf.tmpl'],
                lookup='qutebrowser-config-file-config-conf-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - qutebrowser-config-file-qutebrowser-dir-{{ name }}-managed

{%- set quickmarks = qutebrowser_user.get('quickmarks', {}) %}
qutebrowser-config-file-quickmarks-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/quickmarks
    - source: {{ files_switch([
                  name ~ '-quickmarks.tmpl',
                  'quickmarks.tmpl'],
                lookup='qutebrowser-config-file-quickmarks-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        quickmarks: {{ quickmarks }}
    - require:
      - qutebrowser-config-file-qutebrowser-dir-{{ name }}-managed

{%- set bookmarks = qutebrowser_user.get('bookmarks', {}) %}
qutebrowser-config-file-bookmarks-urls-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/bookmarks/urls
    - source: {{ files_switch([
                  name ~ '-bookmarks.tmpl',
                  'bookmarks.tmpl'],
                lookup='qutebrowser-config-file-bookmarks-urls-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        bookmarks: {{ bookmarks }}
    - require:
      - qutebrowser-config-file-bookmarks-dir-{{ name }}-managed

qutebrowser-config-file-bin-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/bin
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - qutebrowser-config-file-user-{{ name }}-present

{%- set profiles = qutebrowser_user.get('profiles', []) %}
{%- for profile in profiles %}
qutebrowser-config-file-profile-{{ profile }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/bin/{{ profile }}-qutebrowser
    - source: {{ files_switch([
                  name ~ '-launch-qutebrowser-profile.sh.tmpl',
                  'launch-qutebrowser-profile.sh.tmpl'],
                lookup='qutebrowser-config-file-profile-' ~ profile ~ '-' ~ name ~ '-managed',
                )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        profile: {{ profile }}
    - require:
      - qutebrowser-config-file-bin-dir-{{ name }}-managed
{% endfor %}
{% endif %}
{% endfor %}
{% endif %}
