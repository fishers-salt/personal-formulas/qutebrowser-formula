# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as qutebrowser with context %}

{% if salt['pillar.get']('qutebrowser-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_qutebrowser', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

{%- set qutebrowser_user = user.get('qutebrowser', {}) %}
{%- set profiles = qutebrowser_user.get('profiles', []) %}
{%- for profile in profiles %}
qutebrowser-config-clean-profile-{{ profile }}-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/bin/{{ profile }}-qutebrowser
{% endfor %}

qutebrowser-config-clean-bookmarks-urls-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/qutebrowser/bookmarks/urls

qutebrowser-config-clean-quickmarks-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/qutebrowser/quickmarks

qutebrowser-config-clean-config-conf-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/qutebrowser/qutebrowser.conf

qutebrowser-config-clean-keys-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/qutebrowser/keys.conf

qutebrowser-config-clean-config-py-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/qutebrowser/config.py

qutebrowser-config-clean-bookmarks-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/qutebrowser/bookmarks

qutebrowser-config-clean-qutebrowser-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/qutebrowser

{% endif %}
{% endfor %}
{% endif %}
