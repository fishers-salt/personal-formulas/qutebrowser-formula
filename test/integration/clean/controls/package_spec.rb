# frozen_string_literal: true

control 'qutebrowser-package-clean-pkg-removed' do
  title 'should not be installed'

  describe package('qutebrowser') do
    it { should_not be_installed }
  end
end

control 'qutebrowser-package-clean-qutebrowser-profile-script-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/bin/qutebrowser') do
    it { should_not exist }
  end
end
