# frozen_string_literal: true

profiles = %w[default work]
profiles.each do |profile|
  control "qutebrowser-config-clean-profile-#{profile}-auser-absent" do
    title 'should not exist'

    describe file("/home/auser/bin/#{profile}-qutebrowser") do
      it { should_not exist }
    end
  end
end

control 'qutebrowser-config-clean-bookmarks-urls-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/qutebrowser/bookmarks/urls') do
    it { should_not exist }
  end
end

control 'qutebrowser-config-clean-quickmarks-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/qutebrowser/quickmarks') do
    it { should_not exist }
  end
end

control 'qutebrowser-config-clean-config-conf-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/qutebrowser/qutebrowser.conf') do
    it { should_not exist }
  end
end

control 'qutebrowser-config-clean-keys-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/qutebrowser/keys.conf') do
    it { should_not exist }
  end
end

control 'qutebrowser-config-clean-config-py-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/qutebrowser/config.py') do
    it { should_not exist }
  end
end

control 'qutebrowser-config-clean-bookmarks-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/qutebrowser/bookmarks') do
    it { should_not exist }
  end
end

control 'qutebrowser-config-clean-qutebrowser-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/qutebrowser') do
    it { should_not exist }
  end
end
