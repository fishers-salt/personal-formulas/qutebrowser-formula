# frozen_string_literal: true

control 'qutebrowser-package-install-pkg-installed' do
  title 'should be installed'

  describe package('qutebrowser') do
    it { should be_installed }
  end
end

control 'qutebrowser-package-install-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'qutebrowser-package-install-bin-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/bin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'qutebrowser-package-install-qutebrowser-profile-script-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/bin/qutebrowser') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Wrapper around qutebrowser that') }
  end
end
