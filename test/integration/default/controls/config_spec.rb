# frozen_string_literal: true

control 'qutebrowser-config-file-qutebrowser-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/qutebrowser') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'qutebrowser-config-file-bookmarks-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/qutebrowser/bookmarks') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'qutebrowser-config-file-config-py-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/qutebrowser/config.py') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('config.load_autoconfig()') }
  end
end

control 'qutebrowser-config-file-keys-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/qutebrowser/keys.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[!normal]') }
  end
end

control 'qutebrowser-config-file-config-conf-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/qutebrowser/qutebrowser.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ignore-case = smart') }
  end
end

control 'qutebrowser-config-file-quickmarks-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/qutebrowser/quickmarks') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('duckduckgo') }
  end
end

control 'qutebrowser-config-file-bookmarks-urls-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/qutebrowser/bookmarks/urls') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('https://www.startpage.com') }
  end
end

profiles = %w[default work]
profiles.each do |profile|
  control "qutebrowser-config-file-profile-#{profile}-auser-managed" do
    title 'should exist'

    describe file("/home/auser/bin/#{profile}-qutebrowser") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0755' }
      its('content') { should include('# Your changes will be overwritten.') }
      its('content') { should include("$QUTEBROWSER -r #{profile}") }
    end
  end
end
